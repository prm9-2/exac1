package com.example.exac1_java;

import android.widget.Toast;

public class CuentaBanco {
    // Declaración de variables de clase
    private int numCuenta;
    private String nombreCuenta;
    private String banco;
    private float saldo;

    // Constructor
    public CuentaBanco(int numCuenta, String nombreCuenta, String banco, float saldo) {
        this.numCuenta = numCuenta;
        this.nombreCuenta = nombreCuenta;
        this.banco = banco;
        this.saldo = saldo;
    }

    // Métodos de clase
    public void depositar(float dinero) {
        float saldoActualizado = 0;
        this.saldo = this.saldo + dinero;
    }

    public boolean retirar(float dinero) {
        boolean bandera = true;
        if (dinero > this.saldo) {
            bandera = false;
        }

        return bandera;
    }
}


